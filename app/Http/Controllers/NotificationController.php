<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
     private $view_root = 'modules/notification/';
    public function index()
    {
        
            $notifications=\Auth::user()->unreadNotifications;
            //'notifications'=>\Auth::user()->notifications
       

        //dd($data);

       
        $view = view($this->view_root . 'index');
        $view->with('notifications', $notifications);
        return $view;
    }

   
    public function create()
    {
        //
    }

  
    public function store(Request $request)
    {
        //
    }

    
    public function show(\App\Notification $notification)
    {
        $data=json_decode($notification->data);

        if(filter_var($data->url, FILTER_VALIDATE_URL) === FALSE){
            return redirect()->route('notification.index');
        }

        $notification->read_at=\Carbon\Carbon::now();
        $notification->save();

        return redirect()->away($data->url);
    }

    
    public function edit(Notification $notification)
    {
        //
    }

    
    public function update(Request $request, Notification $notification)
    {
        //
    }

   
   public function destroy(\App\Notification $notification){

        $notification->read_at=\Carbon\Carbon::now();
        $notification->save();
        return back();
        
    }
}
