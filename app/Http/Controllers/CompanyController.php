<?php

namespace App\Http\Controllers;

use App\Company;
use Auth;
use Illuminate\Http\Request;
use App\User;
use App\Notifications\TaskCompleteNotification;
use App\Notifications\SimpleNotification;
class CompanyController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

     private $view_root = 'modules/company/';
    public function index()

    {
       
       $users=Company::all();
       $view = view($this->view_root . 'index');
        $view->with('users', $users);
        return $view;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $request->validate([
            'name' => 'required|unique:companies',
            'address' => 'required'
        ]);
        $company = new Company;
        $company->fill($request->all());
        $company->creator_user_id = Auth::id();
        $company->active =true;
        $company->save();
         \App\User::find(1)->notify(
                        new SimpleNotification([
                            'subject'=>'Testing',
                            'sender_id'=>1,
                            'url'=>config('app.url').'/laravel_test/public/company',
                            'message'=>"For Today Delivery Quantity purpose"
                        ])
                    );
         $users=User::find(1);
         $users->notify(new TaskCompleteNotification);
         // Notification::send($users, new TaskCompleteNotification);


        return redirect()->route('company.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(company $company)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, company $company)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(company $company)
    {
        //
    }
}
