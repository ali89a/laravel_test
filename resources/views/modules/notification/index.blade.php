
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Notification</div>

                <div class="card-body">
                    <div class="table-responsive">
                            <table id="datatable-buttons" class="table table-bordered table-hover">
                                <thead class="bg-primary">
                                    <tr>
                                        <th width="25">#</th>
                                        <th>Time</th>
                                        <th>Sender</th>
                                        <th>Subject</th>
                                        <th>Message</th>
                                        <th>Detail</th>
                                        <th>Mark As Read</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($notifications as $row)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ \Carbon\Carbon::parse($row->created_at)->diffForHumans() }}</td>
                                        <td>{{ \App\User::find($row->data['sender_id'])->name }}</td>
                                        <td>{{ $row->data['subject'] }}</td>
                                        <td>{{ $row->data['message'] }}</td>
                                        <td>
                                             <a href="{{route('notification.show', ['notification'=>$row->id])}}" class="btn btn-info" role="button">View Details</a>
                                        </td>
                                        <td>
                                            @if(!$row->read_at)
                                            <a href="{{route('notification.destroy', ['notification'=>$row->id])}}" class="btn btn-info" role="button">Mark As Read</a>
                                            @endif

                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

 